function A=geop(n,alpha,beta,m,varargin)
% geop(n,alpha,beta,m,...)
%   m = dimension
%  'p', prob
%  'edges', number of edges

p = inputParser;
p.addOptional('prob',1);
p.addOptional('edges',[]);
p.addOptional('t',[]);
p.parse(varargin{:});
opt = p.Results;

outputfile = tempname;
mypath = fileparts(mfilename('fullpath'));

opts = sprintf('-o:%s -n:%i -alpha:%f -beta:%f -m:%i ',outputfile,n,alpha,beta,m);
if ~isempty(opt.edges)
    opts = [opts sprintf('-e:%i ',opt.edges)];
else    
    opts = [opts sprintf('-e:%i ',n*n)];
end
if opt.prob < 1
    opts = [opts sprintf('-p:%f ',opt.prob)];
end
if ~isempty(opt.t)
    opts = [opts sprintf('-t:%i ',ceil(n*log(n)*opt.t))];
end
opts
mycmd = [fullfile(mypath,'geop') ' ' opts];

try
    status = system(mycmd);
    edges = load(outputfile);
    A = sparse(edges(:,1)+1,edges(:,2)+1,1,n,n);
    A = A|A';
    A = double(A);
    delete(outputfile)
catch err
    if exist(outputfile,'file')
        delete(outputfile)
    end
    rethrow(err);
end

if exist(outputfile,'file')
    delete(outputfile)
end

