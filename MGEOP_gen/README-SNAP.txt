# DIRECTORY
Go to mitacs/core/examples/mitacs

# COMPILE
make opt (it uses -O4 option)

# USAGE
./geop [options]

options (consist of -<opt name>:<opt val>)
-s:<random seed>	Random number seed (0 implies time-seed, default:1)
-n:<# nodes>		The number of nodes to generate (default:15000)
-e:<# edges>		The number of undirected edges to generate (default:443000)
-m:<# of dims>		The dimension of GEO-P model (default:2)
-alpha:<alpha>		Alpha parameter (default: 0.7) (Attachment Strength to bigger nodes)
-beta:<beta>		Beta parameter (default: 0.15) (Density parameter, from 0, 1- alpha)
-p:<probability>	p parameter, i.e. edge creating probability in the region (default: 1.0)
-t:<time steps>		The size of time steps for simulation (default: 5nlog(n))
-o:<output file>	The name of output file of undirected edge list (if empty, it does not save a graph, default:empty)
-check:<t/f>		Check the completeness of final rankings and some parameter range (default: f)
-sym:<t/f>			Make the graph symmetric (default: f)
-addedge:<t/f>		Add random edges if it generates fewer edges than the give number (not implemented yet)

-var:<variation>	no: no variation / age: add random ages / gnr: random geometric graph (default: no)
-r:<radius>			Radius of random geometric graph (default: 0.3)

EXAMPLES
1) Store undirected edges of GEO-P with n = 15000, e = 443000, alpha = 0.7, beta = 0.15, t = 10000 into 'geop.txt'
$ ./geop -n:15000 -e:443000 -alpha:0.7 -beta:0.15 -t:10000 -o:geop.txt

2) Store undirected edges of random-aged memoryless GEO-P with n = 15000, e = 443000, alpha = 0.7, beta = 0.15, t = 10000 into 'age-geop.txt'
$ ./geop -n:15000 -e:443000 -alpha:0.7 -beta:0.15 -t:0 -var:age -o:age-geop.txt

3) Store both directions of edges of random geometric graph with n = 15000, e = 443000, r = 0.2 into 'gnr.txt'
$ ./geop -n:15000 -e:443000 -sym:t -var:gnr -r:0.2
