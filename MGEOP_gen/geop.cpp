#include "stdafx.h"
#include "Snap.h"

class GeoPParam {
public:
	int n;
	int m;
	int e;
	double alpha;
	double beta;
	double p;
public:
	GeoPParam() {}
};

class GeoPNode {
public:
	int Rank;
	double InfRadius;
	TFltV Pos;

public:
	GeoPNode() {}
	GeoPNode(const int& m, const int& r, const double& R) : Pos(m), Rank(r), InfRadius(R) {}
};
typedef THash<TInt, GeoPNode> TGeoPNodeH;

void Init(const GeoPParam& Param, TIntV& AliveV, PNGraph& G, TGeoPNodeH& NodeH);
double Dist(const TFltV& X, const TFltV& Y);
double GetInfRadius(const int& rank, const GeoPParam& Param);
void GetUnifCoord(TFltV& UniV);
void Join(const int& time, const GeoPParam& Param, PNGraph& G, TGeoPNodeH& NodeH, TIntV& AliveV);
void RemoveRandomEdges(PNGraph& G, const int& ToRemove);
double GetAvgDiam(const PNGraph& G, const int TestNodes);

int main(int argc, char* argv[]) {
	GeoPParam Param;
	Env = TEnv(argc, argv, TNotify::StdNotify);
	const int Seed = Env.GetIfArgPrefixInt("-s:", 1, "Random seed (0 - time seed)");
	Param.n = Env.GetIfArgPrefixInt("-n:", 15000, "# of nodes");
	Param.e = Env.GetIfArgPrefixInt("-e:", 443000, "# of edges");
	Param.m = Env.GetIfArgPrefixInt("-m:", 2, "# of dimensions");
	Param.alpha = Env.GetIfArgPrefixFlt("-alpha:", 0.7, "Alpha");
	Param.beta = Env.GetIfArgPrefixFlt("-beta:", 0.15, "Beta");
	Param.p = Env.GetIfArgPrefixFlt("-p:", 1.0, "Probability");
	const int NSteps = Env.GetIfArgPrefixInt("-t:", int(round(5*Param.n * log(Param.n))), "# of time steps");
	const bool CheckParam = Env.GetIfArgPrefixBool("-check:", false, "Check params with avg diam / completeness of final rankings");
	const bool AddRndEdges = Env.GetIfArgPrefixBool("-addedge:", false, "Add random edges if model generates fewer edges");
	const bool DoSymm = Env.GetIfArgPrefixBool("-sym:", false, "Make it symmetric");
	const TStr OutFNm = Env.GetIfArgPrefixStr("-o:", "", "Outfile name");
	
	TExeTm ExeTm;
	TIntV AliveV;
	TGeoPNodeH NodeH;
	TFlt::Rnd.PutSeed(Seed);
	PNGraph G = new TNGraph(Param.n, Param.e);

	Init(Param, AliveV, G, NodeH);

	for(int t = 0; t < NSteps; t++) {
		const int NewN = t + Param.n;
		const int RndRank = TFlt::Rnd.GetUniDevInt(Param.n) + 1;
		NodeH.AddDat(NewN, GeoPNode(Param.m, RndRank, GetInfRadius(RndRank, Param)));
		GetUnifCoord(NodeH.GetDat(NewN).Pos);
		Join(NewN, Param, G, NodeH, AliveV);
	}
	const int CurEdges = G->GetEdges();
	if(CurEdges < 0.95 * Param.e) {
		fprintf(stderr, "Err: [@m = %d, t = %d, seed = %d] Generated %d edges (fewer than 95%% of %d given edges)\n", Param.m, NSteps, Seed, CurEdges, Param.e);
	}

	int ToRemove = CurEdges - Param.e;
	if(ToRemove > 0) {
		if(ToRemove > 0.05 * Param.e) {
			fprintf(stderr, "Warn: [@m = %d, t = %d, seed = %d] Generated %d edges (more than 105%% of %d given edges)\n", Param.m, NSteps, Seed, CurEdges, Param.e);
		}
		RemoveRandomEdges(G, ToRemove);
		ToRemove = 0;
	} else if(ToRemove < 0) {
		fprintf(stderr, "Warn: [@m = %d, t = %d, seed = %d] Generated %d edges (fewer than %d given edges)\n", Param.m, NSteps, Seed, CurEdges, Param.e);
		if(AddRndEdges) {
		}
	}

	// make it symmetric (undirected)
	if(DoSymm) {
		for(TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++) {
			const int NId = NI.GetId();
			const int OutDeg = NI.GetOutDeg();
			for(int n = 0; n < OutDeg; n++) {
				const int OutNId = NI.GetOutNId(n);
				G->AddEdge(OutNId, NId);
			}
		}
	}
	printf("%d nodes and %d (%s) edges are generated\n", G->GetNodes(), G->GetEdges(), (DoSymm) ? "directed" : "undirected");
	printf("\nrun time: %s (%s)\n", ExeTm.GetTmStr(), TSecTm::GetCurTm().GetTmStr().CStr());

	if(CheckParam) {
		printf("Checking final rankings....");		fflush(stdout);
		TBoolV MarkV(G->GetNodes());
		MarkV.PutAll(false);
		for(TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++) {
			MarkV[NodeH.GetDat(NI.GetId()).Rank-1] = true;
		}
		for(int i = 0; i < MarkV.Len(); i++) {
			if(!MarkV[i]) {  printf("Rank %d is missing\n", i+1);  }
		}
		printf("done\n");		fflush(stdout);
		
		printf("Checking params with avg diam: ");		fflush(stdout);
		const double AvgDiam = GetAvgDiam(G, 1000000);
		printf("1-alpha-beta = %f,  log d/log n = %f\n", 1.0 - Param.alpha - Param.beta, log(AvgDiam) / log(Param.n));
		fflush(stdout);
	}

	if(! OutFNm.Empty()) {
		printf("Saving the graph to output file (%s)....", OutFNm.CStr());		fflush(stdout);
		FILE *fp = fopen(OutFNm.CStr(), "w");
		if(fp == NULL) {
			fprintf(stderr, "Cannot create the file: %s\n", OutFNm.CStr());
			exit(1);
			return -1;
		}
		for(TNGraph::TEdgeI EI = G->BegEI(); EI < G->EndEI(); EI++) {
			const int SrcNId = NodeH.GetDat(EI.GetSrcNId()).Rank - 1;
			const int DstNId = NodeH.GetDat(EI.GetDstNId()).Rank - 1;
			fprintf(fp, "%d\t%d\n", SrcNId, DstNId);
		}
		fclose(fp);
		printf("done\n");
	}
	return 0;
}

void Init(const GeoPParam& Param, TIntV& AliveV, PNGraph& G, TGeoPNodeH& NodeH) {
	AliveV.Gen(Param.n);
	TIntV RndPerm(Param.n, 0);
	for(int n = 0; n < Param.n; n++) {
		AliveV[n] = n;
		G->AddNode(n);
		RndPerm.Add(n + 1);
	}
	RndPerm.Shuffle(TFlt::Rnd);
	for(int n = 0; n < Param.n; n++) {
		const int r = RndPerm[n];
		NodeH.AddDat(n, GeoPNode(Param.m, r, GetInfRadius(r, Param)));
		GeoPNode& Node = NodeH.GetDat(n);
		GetUnifCoord(Node.Pos);
	}
	RndPerm.Clr();

	for(int n1 = 0; n1 < Param.n; n1++) {
		const GeoPNode& Src = NodeH.GetDat(n1);
		for(int n2 = n1 + 1; n2 < Param.n; n2++) {
			const GeoPNode& Dst = NodeH.GetDat(n2);
			if(Dist(Dst.Pos, Src.Pos) < Src.InfRadius) {
				const double Prob = Param.p;
				if(TFlt::Rnd.GetUniDev() <= Prob) {
					G->AddEdge(n1, n2);
				}
			}
		}
	}
}

double Dist(const TFltV& X, const TFltV& Y) {
	double D = 0;
	for(int i  = 0; i < X.Len(); i++) {
		D = TFlt::GetMx(D, TFlt::GetMn(fabs(X[i] - Y[i]), 1.0 - fabs(X[i]-Y[i])));
	}
	return D;
}

void GetUnifCoord(TFltV& UniV) {
	for(int i = 0; i < UniV.Len(); i++) {
		UniV[i] = TFlt::Rnd.GetUniDev();
	}
}

double GetInfRadius(const int& rank, const GeoPParam& Param) {
	return exp((-Param.alpha * log(rank) - Param.beta * log(Param.n)) / double(Param.m)) / 2;
}

void Join(const int& time, const GeoPParam& Param, PNGraph& G, TGeoPNodeH& NodeH, TIntV& AliveV) {
	const int N = Param.n;
	const int RndIdx = TFlt::Rnd.GetUniDevInt(N);
	const int RmTm = AliveV[RndIdx];
	const int RmRank = NodeH.GetDat(RmTm).Rank;

	G->DelNode(RmTm);
	NodeH.DelKey(RmTm);
	
	G->AddNode(time);
	GeoPNode& Node = NodeH.GetDat(time);
	TIntPrV ToUpdateV(N, 0);
	for(TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++) {
		const int SrcTm = NI.GetId();
		if(SrcTm == time) {  continue;  }
		const GeoPNode& Src = NodeH.GetDat(SrcTm);
		if(Dist(Node.Pos, Src.Pos) <= Src.InfRadius) {
//			const double Prob = pow(Src.Rank, -Param.alpha);
			if(TFlt::Rnd.GetUniDev() <= Param.p) {
				G->AddEdge(SrcTm, time);
			}
		}
		int Inc = 0;
		if(Src.Rank > RmRank) {  Inc--;  }
		if(Src.Rank + Inc >= Node.Rank) {  Inc++;  }
		if(Inc != 0) {  ToUpdateV.Add(TIntPr(SrcTm, Inc));  }
	}

	for(int i = 0; i < ToUpdateV.Len(); i++) {
		GeoPNode& Node = NodeH.GetDat(ToUpdateV[i].Val1);
		Node.Rank += ToUpdateV[i].Val2;
		Node.InfRadius = GetInfRadius(Node.Rank, Param);
	}
	ToUpdateV.Clr();
	AliveV[RndIdx] = time;
}

double GetAvgDiam(const PNGraph& G, const int TestNodes) {
	TIntH DistToCntH;
	TBreathFS<PNGraph> BFS(G);
	
	// shotest paths
	TIntV NodeIdV;
	G->GetNIdV(NodeIdV);  NodeIdV.Shuffle(TInt::Rnd);
	for (int tries = 0; tries < TMath::Mn(TestNodes, G->GetNodes()); tries++) {
		const int NId = NodeIdV[tries];
		BFS.DoBfs(NId, true, false, -1, TInt::Mx);
		for (int i = 0; i < BFS.NIdDistH.Len(); i++) {
			DistToCntH.AddDat(BFS.NIdDistH[i]) += 1;
		}
	}
	DistToCntH.SortByKey(true);
	TFltPrV DistNbhsPdfV;
	for (int i = 0; i < DistToCntH.Len(); i++) {
		DistNbhsPdfV.Add(TFltPr(DistToCntH.GetKey(i)(), DistToCntH[i]()));
	}
//	const double EffDiam = TAnf::CalcEffDiamPdf(DistNbhsPdfV, 0.9);
	const double AvgDiam = TSnap::TSnapDetail::CalcAvgDiamPdf(DistNbhsPdfV);
	return AvgDiam;
}

void RemoveRandomEdges(PNGraph& G, const int& ToRemove) {
	const int E = G->GetEdges();
	TIntSet RmSet(ToRemove);
	TIntPrV RmEdges(ToRemove, 0);
	while(RmSet.Len() < ToRemove) {
		const int RndIdx = TFlt::Rnd.GetUniDevInt(E);
		if(!RmSet.IsKey(RndIdx)) {
			RmSet.AddKey(RndIdx);
			RmEdges.Add(TIntPr(RndIdx, RndIdx));
		}
	}
	RmSet.Clr();
	RmEdges.Sort(true);

	int counter = 0, pos = 0;
	for(TNGraph::TEdgeI EI = G->BegEI(); EI < G->EndEI() && pos < ToRemove; EI++) {
		if(counter == RmEdges[pos].Val1()) {
			RmEdges[pos].Val1 = EI.GetSrcNId();
			RmEdges[pos].Val2 = EI.GetDstNId();
			pos++;
		}
		counter++;
	}
	for(int i = 0; i < ToRemove; i++) {
		G->DelEdge(RmEdges[i].Val1, RmEdges[i].Val2);
	}
}
