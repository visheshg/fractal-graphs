#
# File: hw4.py
# Author: Cayman Simpson
# Purpose: CS 224W Homework 4 coding questions
#
import numpy as np
import sys
import os
import snap

def mathematicatize(array):
	return str(array).replace("[","{").replace("]","}").replace("(","{").replace(")","}");

def getAverageDegree(G):
	return float(G.GetEdges())*2/G.GetNodes();

def getAverageClusteringCoefficient(Graph):
	return snap.GetClustCf(Graph, -1)

def getDegreeDist(G):
	v = snap.TIntPrV()
	snap.GetDegCnt(G, v);
	dist = [];

	maxdegree = getMaxDegree(G);
	for i in range(maxdegree + 1): dist.append(0);
	for item in v: dist[item.GetVal1()] = item.GetVal2();

	return dist;

def getMaxDegree(G2):
	return G2.GetNI(snap.GetMxDegNId(G2)).GetDeg();

def getStatistics(filename):
	G = snap.LoadEdgeList(snap.PUNGraph, filename, 0, 1);
	return [getAverageClusteringCoefficient(G), getAverageDegree(G), getDegreeDist(G), snap.GetBfsEffDiam(G, 100, False), G.GetNodes()];

def leastSquares(dist1, dist2):
	length = len(dist1) if len(dist1) > len(dist2) else len(dist2);
	tot = 0;
	for i in range(length):
		a = dist1[i] if i < len(dist1) else 0;
		b = dist2[i] if i < len(dist2) else 0;
		tot += (a - b)*(a - b);
	return tot;

def forestFire(nodes, pf, pb):
	return snap.ConvertGraph(snap.PUNGraph, snap.GenForestFire(nodes, pf, pb));

def trainGraph(clustCoeff, averagedegree, degreeDist, diameter, numNodes, filename):
	#go up diagonal to find when the graph has average degree
	pf = 0;
	pb = 0;
	LR = .05
	increase = True;

	G = forestFire(numNodes, pf, pb); #average degree is constant bbased on nodes
	currad = getAverageDegree(G);
	count = 0;
	while(abs(currad - averagedegree) >= .01 and LR > .0005): #average degree within .01 accuracy or sufficient number of iterations
		if(currad > averagedegree):
			if(increase):
				LR /= 10;
			pf -= LR;
			pb -= LR
			increase = False;
		elif(currad < averagedegree):
			if(not increase):
				LR /= 10;
			pf += LR
			pb += LR
			increse = True;
		else:
			break;
		G = forestFire(numNodes, pf, pb);
		currad = getAverageDegree(G);

	print clustCoeff, getAverageClusteringCoefficient(G);
	print averagedegree, getAverageDegree(G);

	minpf = pf;
	minpb = pb;
	cc = getAverageClusteringCoefficient(G);
	mindif = abs(cc - clustCoeff);

	for i in range(-20,20):
		pf = min(max(0, pf + float(i)/200), .4);
		pb = min(max(0, pb - float(i)/200), .4);

		G = forestFire(numNodes if numNodes < 100000 else 1000, pf, pb); #clustering coefficient is constant based on nodes
		temp = getAverageClusteringCoefficient(G); # leastSquares(degreeDist, getDegreeDist(G));

		#make sure average degree is not off by more than 10% and now train for cc by looking at all options
		print abs(float(getAverageDegree(G))/averagedegree - 1), "\t", pf, "\t", pb
		if(abs(cc - temp) < mindif and abs(float(getAverageDegree(G))/averagedegree - 1) < .1): 
			print "=====" , abs(float(getAverageDegree(G))/averagedegree - 1), "====="
			mindif = abs(cc - temp);
			minpf = pf;
			minpb = pb;

	writeFile = filename[:filename.rfind("/") + 1] + 'FF_' + filename[filename.rfind("/") + 1:];
	os.system("./MGEOP_gen/Snap-2.3/examples/forestfire/forestfire -o:" + writeFile + " -n:" + str(numNodes) + " -f:" + str(minpf) + " -b:" + str(minpb));

def writeGraph(G, filename):
	w = open(filename[:filename.rfind("/") + 1] + 'FF_' + filename[filename.rfind("/") + 1:], 'w');
	w.write("# undirected graph forest fire model for graph: " + filename + "\n");
	w.write("Nodes: " + str(G.GetNodes()) + "Edges: " + str(G.GetEdges()) + "\n");
	for edge in G.Edges():
		w.write(str(edge.GetSrcNId()) + "\t" + str(edge.GetDstNId()) + "\n");

	w.close();


def main(argv):
	[clustCoeff, averagedegree, degreeDist, diameter, nodes] = getStatistics(argv[1]);
	trainGraph(clustCoeff, averagedegree, degreeDist, diameter, nodes, argv[1]);

	sys.exit();

if __name__ == "__main__":
	main(sys.argv);
