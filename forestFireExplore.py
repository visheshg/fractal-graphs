#
# File: hw4.py
# Author: Cayman Simpson
# Purpose: CS 224W Homework 4 coding questions
#
import numpy as np
import sys
import snap
from collections import Counter
import math
import random

def mathematicatize(array):
	return str(array).replace("[","{").replace("]","}").replace("(","{").replace(")","}");

def getMedianPageRank(G):
	PRankH = snap.TIntFltH();
	snap.GetPageRank(G, PRankH);
	tot = sum(PRankH);
	normPR = map(lambda x: float(x)/tot, PRankH);
	normPR = sorted(normPR);
	return normPR[len(normPR)/2];

def getAverageDegree(G):
	return float(G.GetEdges())*2/G.GetNodes();

def getAverageClusteringCoefficient(Graph):
	return snap.GetClustCf(Graph, -1)

def printStatistics(G2, w):
	clust = getAverageClusteringCoefficient(G2);
	if(getAverageDegree(G2) > 50): return;
	w.write("============================================================" + "\n")
	w.write("Number of Nodes:\t\t" + str(G2.GetNodes()) + "\n");
	w.write("Average Degree:\t\t\t" + str(getAverageDegree(G2)) + "\n");
	w.write("Max Node Degree: \t\t" + str(G2.GetNI(snap.GetMxDegNId(G2)).GetDeg()) + "\n")
	w.write("Average Clustering Coeff:\t" + str(clust) + "\n");
	w.write("Median PageRank Value:\t\t" + str(getMedianPageRank(G2)) + "\n");
	w.write("Average Node Value:\t\t" + str(1.0/G2.GetNodes()) + "\n");
	w.write("Effective Diameter:\t\t" + str(snap.GetBfsEffDiam(G2, 30, False)) + "\n");

def test():

	cc = [];
	ad = [];
	m = [];
	d = [];

	for i in range(0,200):
		cc.append(0);
		ad.append(0);
		m.append(0);
		d.append(0);

	i = 0;
	for nodes in range(0,100000,500):
		G = snap.GenForestFire(nodes, .25, .25);

		cc[i] = getAverageClusteringCoefficient(G);
		ad[i] = getAverageDegree(G);
		m[i] = G.GetNI(snap.GetMxDegNId(G)).GetDeg();
		d[i] = snap.GetBfsEffDiam(G, 30, False);
		i += 1;

	w = open('clusteringcoefficient.txt', 'w');
	w.write("ListPlot[");
	w.write(mathematicatize(cc));
	w.write(', Mesh -> None, InterpolationOrder -> 3, ColorFunction -> "SouthwestColors"]');
	w.close();
	w = open('averagedegree.txt', 'w');
	w.write("ListPlot[");
	w.write(mathematicatize(ad));
	w.write(', Mesh -> None, InterpolationOrder -> 3, ColorFunction -> "SouthwestColors"]');
	w.close();
	w = open('maxdegree.txt', 'w');
	w.write("ListPlot[");
	w.write(mathematicatize(m));
	w.write(', Mesh -> None, InterpolationOrder -> 3, ColorFunction -> "SouthwestColors"]');
	w.close();
	w = open('diameter.txt', 'w');
	w.write("ListPlot[");
	w.write(mathematicatize(d));
	w.write(', Mesh -> None, InterpolationOrder -> 3, ColorFunction -> "SouthwestColors"]');
	w.close();

def main(argv):
	test();
	sys.exit();

if __name__ == "__main__":
	main(sys.argv);
