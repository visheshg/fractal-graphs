\begin{thebibliography}{10}

\bibitem{bonato2012}
Anthony Bonato, Jeannette Janssen, and Pawe Prat.
\newblock Geometric protean graphs.
\newblock {\em Internet Math.}, 8(1-2):2--28, 2012.

\bibitem{gephi-data}
GEPHI.
\newblock Datasets.
\newblock \url{https://wiki.gephi.org/index.php/Datasets}, 2014.

\bibitem{skeletons}
Dong-Hee Kim, Jae Noh, and Hawoong Jeong.
\newblock Scale-free trees: The skeletons of complex networks.
\newblock {\em Phys. Rev. E}, 70:046126, Oct 2004.

\bibitem{Kim_thenetwork}
Myunghwan Kim and Jure Leskovec.
\newblock The network completion problem: Inferring missing nodes and edges in
  networks.

\bibitem{hgpaper}
Dmitri Krioukov, Fragkiskos Papadopoulos, Maksim Kitsak, Amin Vahdat, and
  Mari\'an Bogu\~n\'a.
\newblock Hyperbolic geometry of complex networks.
\newblock {\em Phys. Rev. E}, 82:036106, Sep 2010.

\bibitem{leskovec-kronecker}
Jure Leskovec, Deepayan Chakrabarti, Jon Kleinberg, Christos Faloutsos, and
  Zoubin Ghahramani.
\newblock Kronecker graphs: An approach to modeling networks.
\newblock {\em J. Mach. Learn. Res.}, 11:985--1042, Mar 2010.

\bibitem{shrinkingdiameter}
Jure Leskovec, Jon Kleinberg, and Christos Faloutsos.
\newblock Graph evolution: Densification and shrinking diameters.
\newblock {\em ACM Trans. Knowl. Discov. Data}, 1(1), March 2007.

\bibitem{bigclam}
Jure Leskovec, Kevin~J. Lang, Anirban Dasgupta, and Michael~W. Mahoney.
\newblock Community structure in large networks: Natural cluster sizes and the
  absence of large well-defined clusters, 2008.

\bibitem{locci2009computing}
Mario Locci, Giulio Concas, and Ivana Turnu.
\newblock Computing the fractal dimension of software networks.
\newblock In {\em Proceedings of the 9th WSEAS international conference on
  Applied computer science}, pages 146--151. World Scientific and Engineering
  Academy and Society (WSEAS), 2009.

\bibitem{makse-data}
Hernan Makse.
\newblock Datasets.
\newblock \url{http://www-levich.engr.ccny.cuny.edu/~hmakse/soft_data.html},
  2014.

\bibitem{snap-data}
Stanford Snap.
\newblock Datasets.
\newblock \url{http://snap.stanford.edu/data}, 2014.

\bibitem{song2007calculate}
Chaoming Song, Lazaros~K Gallos, Shlomo Havlin, and Hern{\'a}n~A Makse.
\newblock How to calculate the fractal dimension of a complex network: the box
  covering algorithm.
\newblock {\em Journal of Statistical Mechanics: Theory and Experiment},
  2007(03):P03006, 2007.

\bibitem{song2005self}
Chaoming Song, Shlomo Havlin, and Hernan~A Makse.
\newblock Self-similarity of complex networks.
\newblock {\em Nature}, 433(7024):392--395, 2005.

\end{thebibliography}
