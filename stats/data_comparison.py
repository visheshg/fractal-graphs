import snap
import sys
from os import listdir
from os.path import isfile, join
import time

# Purpose: Given a graph, get the average clustering coefficient as defined by Watts and Strogatz
# Input a Graph, get back a float of the average clustering coefficient.
def getAverageClusteringCoefficient(Graph):
	return snap.GetClustCf(Graph, -1)

# Purpose: Get histogram data of clustering coefficients of all the nodes in the Graph
# Input: Graph (will be considered undirected) as well as the desired number of bins
# Output = hist: a list of the number of nodes which fall into each bin (i.e. a histogram)
# bin_edges: a list (of len(hist) + 1) specifying the boundaries of each bin, which define hist
def getClusteringCoefficientDistribution(Graph, bins = 10):
	NIdCCfH = snap.TIntFltH()
	snap.GetNodeClustCf(Graph, NIdCCfH)
	cc_by_node_id = []
	for item in NIdCCfH:
		cc_by_node_id.append(NIdCCfH[item])
	[hist, bin_edges] = np.histogram(cc_by_node_id,bins)
	return [hist,bin_edges]

# Purpose: Get histogram data of node in-degrees for Graph
# Input: Graph; and Output: A List of (x,y) pairs where x = Overall node in-degree, and y = # of nodes with this in-degree
def getAverageInDegree(Graph): #todo
	DegToCntV = snap.TIntPrV()
	in_degree_histogram = []
	snap.GetInDegCnt(Graph,DegToCntV)
	for item in DegToCntV:
		in_degree_histogram.append((item.GetVal1(),item.GetVal2()))

	average = 0;
	for i in range(len(in_degree_histogram)):
		average += i*in_degree_histogram[i][1];

	return float(average)/Graph.GetNodes();

# Purpose: Get histogram data of node out-degrees for Graph
# Input: Graph; and Output: A List of (x,y) pairs where x = Overall node out-degree, and y = # of nodes with this out-degree
def getAverageOutDegree(Graph): #todo
	DegToCntV = snap.TIntPrV()
	out_degree_histogram = []
	snap.GetOutDegCnt(Graph,DegToCntV)

	for item in DegToCntV:
		out_degree_histogram.append((item.GetVal1(),item.GetVal2()))

	average = 0;
	for i in range(len(out_degree_histogram)):
		average += i*out_degree_histogram[i][1];

	return float(average)/Graph.GetNodes();

# Purpose: Use this to get the average IN-DEGREE OR OUT-DEGREE OR OVERALL DEGREE for a graph
# Input: A List of (x,y) pairs (i.e. histogram data), which is either the output of getOutDegreeDistribution,
# getInDegreeDistribution, or getDegreeDistribution
# Output: A float representing the average of the specified degree for the graph (depends on the input data)
def getAverageDegree(G): #todo
	return float(G.GetEdges())*2/G.GetNodes();

# Purpose: Use this to get the histogram data on all WCC sizes in a given Graph
# Input: Graph
# Output: List of (x,y) pairs where x = the size of the WCC, y = # of WCCs of size x
def getWCCDistribution(Graph):
	ComponentDist = snap.TIntPrV()
	wcc_component_histogram = []
	snap.GetWccSzCnt(Graph,ComponentDist)
	for comp in ComponentDist:
		wcc_component_histogram.append((comp.GetVal1(),comp.GetVal2()))
	return sorted(wcc_component_histogram)

# Purpose: Use this to get the histogram data on all SCC sizes in a given Graph
# Input: Graph
# Output: List of (x,y) pairs where x = the size of the SCC, y = # of SCCs of size x
def getSCCDistribution(Graph):
	ComponentDist = snap.TIntPrV()
	scc_component_histogram = []
	snap.GetSccSzCnt(Graph,ComponentDist)
	for comp in ComponentDist:
		scc_component_histogram.append((comp.GetVal1(),comp.GetVal2()))
	return sorted(scc_component_histogram)

def getAverage(dist):
	tot = 0;
	num = 0;
	for pair in dist:
		tot += pair[1]*pair[0];
		num += pair[1]
	return float(tot)/num;

def getMedianPageRank(G):
	PRankH = snap.TIntFltH();
	snap.GetPageRank(G, PRankH);
	tot = sum(PRankH);
	normPR = map(lambda x: float(x)/tot, PRankH);
	normPR = sorted(normPR);
	return normPR[len(normPR)/2];

def main(argv):

	# real_world = snap.LoadEdgeList(snap.PUNGraph,"ca-GrQc.txt", 0, 1)
	# email = snap.LoadEdgeList(snap.PNGraph,"email_network.txt", 0, 1)
	# Graph = snap.GenRndGnm(snap.PUNGraph, 100, 1000)
	directedGraphs = [f for f in listdir("data/Directed") if isfile(join("data/Directed",f)) and f.endswith(".txt")];
	for filename in directedGraphs:
		start = time.clock();
		G = snap.LoadEdgeList(snap.PNGraph, "data/Directed/" + filename, 0, 1);
		print "-------------------------" + filename + "--------------------------"
		print "Number of Nodes:\t\t" + str(G.GetNodes());
		print "Average In-Degree:\t\t" + str(getAverageInDegree(G));
		print "Average Out-Degree:\t\t" + str(getAverageOutDegree(G));
		print "Average WCC-Distribution:\t" + str(getAverage(getWCCDistribution(G)));
		print "Average SCC-Distribution:\t" + str(getAverage(getSCCDistribution(G)));
		print "Average Clustering Coeff:\t" + str(getAverageClusteringCoefficient(G));
		print "Median PageRank Value:\t\t" + str(getMedianPageRank(G));
		print "Average Node Value:\t\t" + str(1.0/G.GetNodes());
		print "Total time (in seconds):\t" + str(time.clock() - start);

	print ("\n\n------------------------------------------------------------------------------------------------------------------------------------------------------\n" +
	"------------------------------------------------------------------------------------------------------------------------------------------------------\n" + 
	"------------------------------------------------------------------UNDIRECTED GRAPHS-------------------------------------------------------------------\n" + 
	"------------------------------------------------------------------------------------------------------------------------------------------------------\n" +
	"------------------------------------------------------------------------------------------------------------------------------------------------------\n\n");


	undirectedGraphs = [ f for f in listdir("data/Undirected") if isfile(join("data/Undirected",f)) and f.endswith(".txt")];

	for filename in undirectedGraphs:
		start = time.clock();
		G = snap.LoadEdgeList(snap.PUNGraph, "data/Undirected/" + filename, 0, 1);

		print "-------------------------" + filename + "--------------------------"
		print "Number of Nodes:\t\t" + str(G.GetNodes());
		print "Average Degree:\t\t\t" + str(getAverageDegree(G));
		print "Average WCC-Distribution:\t" + str(getAverage(getWCCDistribution(G)));
		print "Average Clustering Coeff:\t" + str(getAverageClusteringCoefficient(G));
		print "Median PageRank Value:\t\t" + str(getMedianPageRank(G));
		print "Average Node Value:\t\t" + str(1.0/G.GetNodes());
		print "Total time (in seconds):\t" + str(time.clock() - start);




if __name__ == "__main__":
	main(sys.argv);










