#!/usr/bin/env python
# encoding: utf-8
# filename: profile.py

import pstats, cProfile

import stats as s
import boxcount as bc
from intbitset import intbitset as ibs
o = s.outmap(s.slurpGraph('../data/as20000102/real/as20000102.dat'))
cProfile.runctx("bc.boxcount(o,8,True)", globals(), locals(), "Profile.prof")

s = pstats.Stats("Profile.prof")
s.strip_dirs().sort_stats("time").print_stats()





