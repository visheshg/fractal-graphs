from subprocess import call
from os import listdir
from os.path import isfile, join
import os
import sys
import inspect
import snap
import math
import numpy as np


def getThisPath():
	return os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) # script directory

def getPowerLawCoefficient(data):

	x = filter(lambda x: data[x] != 0, range(len(data)));
	x = map(lambda x: math.log(x), x);

	data = filter(lambda x: x != 0, data);

	data = map(lambda x: math.log(x), data);

	A = np.vstack([x, np.ones(len(x))]).T;
	return np.linalg.lstsq(A, data)[0]



def main(argv):
	#call(["ls", "-l"]);
	firstFile = [ f for f in listdir(getThisPath()) if isfile(join(getThisPath(),f)) and f[0] is not "." and "_" not in f][0];

	if( "Undirected" not in getThisPath()):
		print "Can only generate graphs for undirected graphs";
		sys.exit();
	
	print firstFile
	G = snap.LoadEdgeList(snap.PUNGraph, firstFile) 
	
	numNodes = G.GetNodes();
	numEdges = G.GetEdges();

	DegToCntV = snap.TIntPrV()
	snap.GetDegCnt(G, DegToCntV)

	array = [0]*(G.GetNI(snap.GetMxDegNId(G)).GetDeg() + 1);
	for item in DegToCntV:
   		array[item.GetVal1()] = item.GetVal2();

	alpha = getPowerLawCoefficient(array)[0];
	averageDegree = numEdges*2/numNodes;

	GPalpha = (alpha - 1)**-1;


	# the two is the jiggly thing that we ahve to manually correct
	GPbeta = -(math.log((1-GPalpha)*averageDegree/2)/math.log(numNodes) + 1 + GPalpha);


	clustcoeff = snap.GetClustCf(G, -1);
	# for HG - TEMP
	temp = 0 if clustcoeff >= .6 else 29.16666667*clustcoeff*clustcoeff - 34.16666667*clustcoeff  + 10

	GPalpha = float(int(GPalpha*1000))/1000;
	GPbeta = float(int(GPbeta*1000))/1000;
	alpha = float(int(alpha*1000))/1000;



	#print intermediary and then modify it
	
	# call(["/Users/caymansimpson/Repositories/fractal-graphs/MGEOP_gen/geop",
	# 			"-n:" + str(numNodes),
	# 			"-e:" + str(numEdges),
	# 			"-alpha:" + str(GPalpha),
	# 			"-beta:" + str(GPbeta),
	# 			"-o:" + firstFile.replace(".txt","") + "_MGEOP.txt"])

	# print "Done with geop"

	call(["/Users/caymansimpson/Repositories/fractal-graphs/HYPERBOLIC_gen/tools/hyperbolic_graph_generator",
				"-n " + str(numNodes),
				"-k " + str(averageDegree),
				"-g " + str(alpha) if alpha >=2 else "2",
				"-t " + str(temp),
				"-o " + getThisPath()])

	#print geopCall;
	#print hgCall;

	data = [];
	with open("graph.hg") as f:
		data = ("#" + f.read()).split("\n");

	data = filter(lambda x: len(x.split("\t")) != 3, data);
	print data;
	try:
		w = open(firstFile.replace(".txt","") + "_HG.txt", 'w');
		for x in data:
			w.write(x + "\n")
		w.close();
		#call(["rm","graph.hg"])
	except IOError:
		print "Could not write HG graph to file.";

if __name__ == "__main__":
	main(sys.argv);