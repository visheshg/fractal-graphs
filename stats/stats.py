#! /usr/local/bin/python
# cython: profile=True, infer_types=True, boundscheck=False, wraparound=False, cdivision=False
# Vishesh Gupta, Cayman Simpson, Bharad Raghavan
# CS224W, Created: 13 October 2014
# file: stats.py

# The purpose of this file is to take in a graph file in snap input format (2 cols
# from and to for each edge) and output a series of aggregate statistics about the
# graph that we use to compare against our generated models.
# The graph can either be a generated graph or a real graph.
#
# Output will be a series of stats on the command line along with a series of
# distributions saved to the outfile. If no outfile is given, a file named
# infile_dists.dat will be created.
# Included statistics are:
# 1)  Diameter
# 2)  Shrinking Diameter
# 3)  Box-counting
# 4)  Skeleton
# 5)  Degree Distribution
# 6)  Clustering Coefficient Distribution
# 7)  WCC Distribution
# 8)  SCC Distribution
# 9)  Shortest Path Distribution
# 10) Distribution of Communities by size, clustering coefficient.
# 11) SCREE Plot
# 12) Hop Plot
# 13) (Edge) Betweeness Centrality Distribution (if feasible)
#        - somewhat captured in the Skeleton model.
# 14) Pagerank Distribution.
# usage: python stats.py infile [outfile]

import os
import sys
import snap
import getopt
from cytoolz import *
from itertools import izip_longest as zipl
from intbitset import intbitset as ibs
import random
import numpy as np
import math
from bidict import bidict
import graph_basic_stats as gbs
import boxcount as bc
import subprocess

# -------------------------------- File and Console I/O ----------------------
def dumpEdges(edges):
  ''' takes a collection of edges and prints it to the console'''
  for e in edges:
    print str(e[0]) + '\t' + str(e[1])

def dumpOutmap(outmap):
  '''takes a outmap like {100: set([1211 1231 2]) ...}
     and dumps it to console. '''
  for x,y in concat([[(k,x) for x in v] for (k,v) in outmap.items()]):
    print str(x) + '\t' + str(y)

def writeOutmap(filename, outmap):
  ''' takes a outmap like {100: set([1211 1231 2]) ...} and dumps it out to file. '''
  f = open(filename, 'w')
  for x,y in concat([[(k,x) for x in v] for (k,v) in outmap.items()]):
    f.write(str(x) + '\t' + str(y) + '\n')
  f.close()

def slurpGraph(filename):
  ''' reads in the ENTIRE graph file and returns list of [source, target] '''
  return [[int(y) for y in x] for x in filter(
      lambda x: x[0].isdigit(),
      [x.strip().split('\t') for x in open(filename)])]

#--------------------------  Graph Data Transformations --------------------------

def outmap(edges):
  '''This takes edges like [source target] and groups them into a neighbor map
     like {source: set(t1, t2, t3...)}.'''
  return merge_with(partial(reduce, ibs.__or__),
           reduceby(first, lambda acc, v: acc | ibs([v[1]]), edges, ibs()),
           reduceby(second, lambda acc,v: acc | ibs([v[0]]), edges, ibs()))

def nodes(edges):
  '''computes the set of nodes given the set of edges.
     storing edges is good for some things, but computing the nodes is an
     expensive operation'''
  return set(list(pluck(1,edges)) + list(pluck(0,edges)))

def edges(outmap):
  return concat([[(k,x) for x in v] for (k,v) in outmap.iteritems()])

def snapedges(graph):
  return [(e.GetSrcNId(), e.GetDstNId()) for e in graph.Edges()]

# -------------------------------- Skeleton --------------------------------------
def betweennessSnap(graph):
  '''takes a snap style PUNGraph (MUST BE UNDIRECTED) and returns
     two lists of the betweenness centrality for each node and edge.'''
  nbtwn = snap.TIntFltH()
  ebtwn  = snap.TIntPrFltH()
  snap.GetBetweennessCentr(graph, nbtwn, ebtwn, 1.0)
  return ([((e.Val1.Val, e.Val2.Val), ebtwn[e]) for e in ebtwn],
          [(e, nbtwn[e]) for e in nbtwn])

def skeleton(graph):
  ''' does kruskal's algorithm maximizing edge betweenness'''
  ec, nc = betweennessSnap(graph)
  # add edges starting from most centrality.
  ec = sorted(ec, key=lambda x: -x[1])

  skeleton = set() # set of edges
  forests = dict(map(reversed, enumerate(pluck(0,nc))))
  for e in ec:
    edge = e[0]
    if forests[edge[0]] != forests[edge[1]]:
      skeleton.add(edge)
      forests = valmap(lambda v: forests[edge[0]] if v == forests[edge[1]] else v,
                      forests)
  return skeleton

# ----------------------------------  BFS helpers --------------------------------
def expansion(outmap, nodes):
  '''nodes is a set of nodes. Returns a set of all the nodes that can be
     reached from this set of nodes. '''
  return reduce(set.union,
                [outmap.get(i) for i in nodes if i in outmap],
                set())

def nhop(graph, nid, hop):
  NodeVec = snap.TIntV()
  snap.GetNodesAtHop(graph, nid, hop, NodeVec, False)
  return {i for i in NodeVec}

def bfsn(graph, nid, hops):
  '''Computes a BFS of length n from start given outmap as the graph model.
     if next, will return the expansion of that set as well.'''
  return reduce(set.union,
                [nhop(graph, nid, h) for h in range(1, hops)],
               set())

# -------------------------------- Cluster Growing Method ---------------------------
def computeBoxes(graph, nids, boxsize):
  nodes = set(nids)
  ''' random box algorithm - pick seed node and do boxsize bfs from the node,
      then make more seed nodes, etc until entire graph is covered. '''
  boxes = {}
  bid = 1
  while len(nodes) > 0:
    n = random.sample(nodes, 1)[0]
    box = bfsn(graph, n, boxsize)
    box.add(n)
    box = box & nodes
    nodes -= box
    for n in box:
      boxes[n] = bid
    bid += 1

  return boxes


def fractalDimension(graph, nodes, boxsize=2):
  '''Fractal dimension is <clustermass> ~ boxsize^dimension
     so fit a line to log/log data, take slope'''
  pointsa = keyfilter(lambda k: k > 1,
                     frequencies(map(len, computeBoxes(
                       graph, nodes, boxsize)))).items()
  points = np.matrix(pointsa)
  A = np.vstack([map(math.log, points[:,0]), np.ones(len(points))]).T
  m,c = np.linalg.lstsq(A, map(math.log, points[:,1]))[0]
  return [-m, pointsa]

# -------------------------------- Graph Reduction -----------------------
def invertdists(dists):
  inv_map = {}
  for (k,v) in dists.iteritems():
    inv_map.setdefault(v, ibs()).add(k)
  return inv_map

def ibsexpansion(outmap,  nodes):
  return reduce(ibs.__ior__,
               get(list(nodes), outmap, ibs()),
               ibs())

def reduceBoxes(outmap, ntobox):
  '''Takes a bunch of boxes and returns the reduced graph created by adding
     edges between boxes that have a connection in the original graph.'''

  boxindexes = bidict(enumerate(invertdists(ntobox).values()))
  return dict(filter(lambda (k,v): len(v) > 0 and k not in v, map(
        lambda (k,v): (k, {ntobox[i] for i in ibsexpansion(outmap,v)}),
        boxindexes.iteritems())) )

#TODO(visheshg): Is this function really useful? Oh well.
def reduceGraph(graph, boxsize):
  '''convenience function that returns a new random box-based on the edges
     by computing the boxes for you directly.'''

  return reduceBoxes(outmap(snapedges(graph)),
                     computeBoxes(graph, nodes(snapedges(graph)), boxsize))

# -------------------------------- PageRank ---------------------------

def pagerank(G, beta=.85,iterations=40,R=5):
  visits = [0]*G.GetMxNId();

  for node in G.Nodes():
    currentNode = node.GetId();
    for j in range(R):
      visits[currentNode] += 1;
      while(random.random() <= beta):
        empty = True;

        rand = random.randint(0, G.GetNI(currentNode).GetOutDeg());

        count = 0;
        for edge in G.GetNI(currentNode).GetOutEdges():
          empty = False;
          if(rand == count):
            currentNode = G.GetNI(currentNode).GetNbrNId(rand);

          count += 1;

        if(empty): break;

        visits[currentNode] += 1;

  return map(lambda x: x*(1-beta)/(G.GetNodes()*R), visits);

# ------------------------------- Running the Harness ------------------------
def usage():
  # TODO(vishesh): do we need to actually include weights?
  print 'usage: python -[bdrsv] stats.py infile, [infile...]'
  print ''
  print '   -b: boxsize, default 2'
  print '   -d: graph is directed. omit for undirected'
  print '   -r: reduce a set of edges by random box-covering'
  print '   -s: get the graph skeleton'
  print '   -v: verbose mode'

def formatdist(dist):
  return '{' + ', '.join(map(
                           lambda x: '{' + ', '.join(map(str, x)) + '}',
                           dist)) + '}'
MIN_BOX = 3
def main():
  opts, args = getopt.getopt(sys.argv[1:], 'b:drsva')
  verbose = False
  boxsize = MIN_BOX
  isDirected = False
  for o,a in opts:
    if o == '-b':
      boxsize = int(a)
      if boxsize < MIN_BOX: boxsize = MIN_BOX
    if o == '-v':
      verbose = True
    if o == '-d':
      isDirected = True
    if o == '-r':
      if len(args) < 2:
        usage()
        print '-r requires at least two inputs'
        return
      dumpOutmap(reduceGraph(snap.LoadEdgeList(snap.PNGraph, args[0]), int(args[1])))
      return
    if o == '-s':
      if len(args) < 1:
        usage()
        print '-s requires 1 argument (filename)'
      else: dumpEdges(skeleton(snap.LoadEdgeList(snap.PUNGraph, args[0])))
      return
    if o == '-a':
      # reduceall option - does boxcounting up to max and writes out all new boxes.
      if len(args) < 1:
        usage()
        print '-a requires 2 args [filename outdir] and -b option'
      else:
        fname = os.path.basename(args[0]).split('.')[0]
        omap = outmap(slurpGraph(args[0]))
        # boxsize +1 because it's exclusive, but bash in general is inclusive.
        for (k,v) in bc.boxcount(omap, boxsize+1, True).iteritems():
          path = os.path.join(args[1], fname + '_' + str(k) + 'r.txt')
          writeOutmap(path, reduceBoxes(omap,v))
          print path
      return


  if len(args) < 1:
    usage()
    return

  if len(args) > 1: print '{'
  for e in args:
    dumpStats(e, verbose, boxsize, isDirected)
    if len(args) > 1: print ','
  if len(args) > 1: print '}'

def dumpStats(filename, verbose, boxsize, isDirected):
  graph = snap.LoadEdgeList(snap.PNGraph, filename)
  edges = snapedges(graph)
  nids = nodes(edges)
  if verbose: print 'graph [',filename,'] loaded...'

  degdist = gbs.getDegreeDistribution(graph)
  if isDirected:
    outdegdist = gbs.getOutDegreeDistribution(graph)
    indegdist = gbs.getInDegreeDistribution(graph)
  if verbose: print 'degree distributions done.'
  ccdegdist = gbs.getClusteringCoefficientDistributionByDegree(graph)
  if verbose: print 'degree clustering coefficients done.'
#  girvnewdist = gbs.getGirvanNewmanDistribution(
#    snap.ConvertGraph(snap.PUNGraph, graph))
  prdist = frequencies(pagerank(graph, beta=.85, iterations=40, R=5));
  if verbose: print 'pagerank distributions done.'

  # these ones have bin edges too - not sure how to store yet.
  tridist = gbs.getNodeTriangleParticipation(graph)
  if verbose: print 'node triangles done.'
  # TODO(visheshg): betweenness centrality distribution

  # --- box counting fractality - now based on boxcount.pyx (greedy boxcount)
#  m, points = fractalDimension(graph, nids, boxsize)
  boxes = valmap(lambda v: frequencies(frequencies(v.values()).values()),
                 bc.boxcount(outmap(edges), boxsize, False))
  if verbose: print 'boxcount done.'

  # community partitions!
  communities = subprocess.check_output(['./analyzeinfomap', 
                                        filename.split('.')[0]+'.tree'])

  print '{'
  print '{"metadata","name", "',filename,'"},'
  print '{"stat","nodes", ', graph.GetNodes() , '},'
  print '{"stat","edges", ', graph.GetEdges() , '},'
  print '{"stat","diameter", ', gbs.getDiameter(graph, 10, isDirected), '},'
  print '{"average","clustering-coeff", ', gbs.getAverageClusteringCoefficient(graph), '},'
  print '{"average","avg_degree", ', gbs.getAverageDegree(degdist) , '},'
  if isDirected:
    print '{"average","avg_out-degree", ', gbs.getAverageDegree(outdegdist) , '},'
    print '{"average","avg_in-degree", ', gbs.getAverageDegree(indegdist) , '},'
  print '{"max","max_wcc", ', snap.GetMxWccSz(graph) , '},'
  print '{"max","max_scc", ', snap.GetMxSccSz(graph) , '},'
  print '{"stat","boxsize", ',boxsize,'},'

  print '{"dist","degree", ',  formatdist(degdist) , '},'
  if isDirected:
    print '{"dist","out-degree", ', formatdist(outdegdist) , '},'
    print '{"dist","in-degree", ', formatdist(indegdist) , '},'
  print '{"dist", "node-triangles", ', formatdist(tridist) ,'},'
  print '{"dist","clustering-degree", ', formatdist(ccdegdist) , '},'
  print '{"dist","pagerank", ', formatdist(prdist.items()) + '},'
  print '{"dists","boxcount", ',formatdist(
    valmap(lambda v:formatdist(v.iteritems()), boxes).iteritems()),'},'
  print '{"dists","infomap", ',communities,'}'
  print '}'

if __name__ == '__main__':
  main()





























