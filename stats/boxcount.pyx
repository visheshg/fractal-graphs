#! /usr/local/bin/python
# cython: profile=True, infer_types=True, boundscheck=False, wraparound=False, cdivision=False
# Vishesh Gupta, Cayman Simpson, Bharad Raghavan
# CS224W, Created: 20 November 2014
# file: boxcount.pyx

from cytoolz import *
from intbitset import intbitset as ibs

cdef inline ibsexpansion(dict outmap,  nodes):
  return reduce(ibs.__ior__,
               get(list(nodes), outmap, ibs()),
               ibs())
cdef enum:
  MIN_BOX = 3

cdef enum:
  MAX_BOX = 20

def bfsgen(dict outmap, int nid, int hops):
  cdef int i
  visited = ibs()
  fringe = ibs([nid])
  for i in range(hops):
    visited |= fringe
    fringe = ibsexpansion(outmap, fringe) - visited
    if len(fringe) == 0: break
    yield fringe

cdef list remgen(dict outmap, int nid, int hops, prevnids):
  cdef int i
  visited = ibs()
  restricted = prevnids
  fringe = ibs([nid])
  
  cdef list res = [ibs() for i in range(MAX_BOX)]
  res[0].__ior__(restricted)
  for i in range(1,hops):
    visited |= fringe
    restricted -= fringe
    fringe = ibsexpansion(outmap, fringe).__isub__(visited)
    if len(fringe) == 0 or len(restricted) == 0: break
    res[i].__ior__(restricted)

  return res

cdef inline list orearrangeIds(dict outmap):
  return list(cons(0, concat(bfsgen(outmap, 0, len(outmap)))))

cdef inline int color(str bits):
  cdef int c = bits.find('0')
  return len(bits) if c == -1 else c

cdef inline str restrictedset(dict lcolormap, nodes):
  cdef int n
  i = ibs()
  for n in nodes:
    i.__ior__(lcolormap[n])
  return i.strbits()

def boxcount(dict omap, int mbox, bint verbose):
  cdef int i, c, l
  cdef list colors
  cdef list onids = orearrangeIds(omap)
  cdef dict colormap = {l:{onids[0]:ibs([0])} for l in range(MIN_BOX,mbox+1)}
  for i in range(1,len(onids)):
    # accumulate the merged sets moving outwards -
    # for l = 2 we are also not considering anything with l=1
    # then for each disk of r <= l, we can get the box color from the map.
    colors = list(map(
        lambda t: (t[0], color(restrictedset(colormap[t[0]], t[1])) ),
        zip(range(MIN_BOX,mbox+1),
               drop(MIN_BOX-1, remgen(omap, onids[i], mbox, ibs(onids[:i]))) )
      ))
    if verbose and i%1024 == 0: print i
    for (l,c) in colors:
      colormap[l][onids[i]] = ibs([c])

  return valmap(lambda v: valmap(lambda x: x.extract_finite_list()[0],v), colormap)






















