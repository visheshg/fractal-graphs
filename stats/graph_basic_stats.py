import snap
import collections
from operator import itemgetter
import numpy as np


# This method generates data that can be plotted as a Hop Plot. In a hop plot,
# we measure the fraction of connected pairs that are reachable within h hops.
# Input: the Graph, max_dist (the maximum number of hops you want to explore),
# isDir (bool for whether the graph directed or not), and kApprox (an integer that
# must be a multiple of 8) which indicates how precise you want to be
# Output: a list of pairs (x,y) where x is the hop distance and y are the approximate
# number of connected pairs reachable from distance x or fewer.
# DON'T USE THIS FUNCTION YET, it gives weird estimates.
def getHopPlot(Graph, max_dist, isDir, kApprox=32):
    DistNbrsV = snap.TIntFltKdV()
    snap.GetAnf(Graph, DistNbrsV, max_dist, isDir, kApprox)
    hopPlotHistogram = []
    for i in range(len(DistNbrsV)):
        hopPlotHistogram.append((i + 1, DistNbrsV[i].Dat()))
    return hopPlotHistogram
# Given a graph, an int k, and a boolean on whether or not the graph is undirected
# construct an adjacency matrix based off the edges of the graph, and
# return the top k eigenvalues


def getEigenValues(Graph, k=4, undirected=True):
    adjacency_matrix = np.zeros((Graph.GetNodes(), Graph.GetNodes()))
    for edge in Graph.Edges():
        adjacency_matrix[edge.GetSrcNId(), edge.GetDstNId()] += 1
        if undirected:
            adjacency_matrix[edge.GetDstNId(), edge.GetSrcNId()] += 1
    eig_vals, eig_vecs = np.linalg.eig(adjacency_matrix)
    return sorted(list(eig_vals))[-k:]

# Purpose: Get histogram data of node-degrees (overall degree), for Graph
# Input: Graph; and Output: A List of (x,y) pairs where x = Overall node
# degree, and y = # of nodes with this degree


def getDegreeDistribution(Graph):
    DegToCntV = snap.TIntPrV()
    degree_histogram = []
    snap.GetDegCnt(Graph, DegToCntV)
    for item in DegToCntV:
        degree_histogram.append((item.GetVal1(), item.GetVal2()))
    return sorted(degree_histogram)

# Purpose: Get histogram data of node in-degrees for Graph
# Input: Graph; and Output: A List of (x,y) pairs where x = Overall node
# in-degree, and y = # of nodes with this in-degree


def getInDegreeDistribution(Graph):
    DegToCntV = snap.TIntPrV()
    in_degree_histogram = []
    snap.GetInDegCnt(Graph, DegToCntV)
    for item in DegToCntV:
        in_degree_histogram.append((item.GetVal1(), item.GetVal2()))
    return sorted(in_degree_histogram)

# Purpose: Get histogram data of node out-degrees for Graph
# Input: Graph; and Output: A List of (x,y) pairs where x = Overall node
# out-degree, and y = # of nodes with this out-degree


def getOutDegreeDistribution(Graph):
    DegToCntV = snap.TIntPrV()
    out_degree_histogram = []
    snap.GetOutDegCnt(Graph, DegToCntV)
    for item in DegToCntV:
        out_degree_histogram.append((item.GetVal1(), item.GetVal2()))
    return sorted(out_degree_histogram)


# Purpose: Get histogram data of the average clustering coefficients of the nodes in the graph based on their degree
# Input: a Graph (will be considered undirected)
# Output: A list of (x,y) pairs (cc_by_degree_data) where x = Node Degree
# and y = Avg CC for all nodes of degree x
def getClusteringCoefficientDistributionByDegree(Graph):
    CfVec = snap.TFltPrV()
    Cf = snap.GetClustCf(Graph, CfVec, -1)
    cc_by_degree_data = []
    for pair in CfVec:
        cc_by_degree_data.append((pair.GetVal1(), pair.GetVal2()))
    return sorted(cc_by_degree_data)

# Purpose: Get histogram data of clustering coefficients of all the nodes in the Graph
# Input: Graph (will be considered undirected) as well as the desired number of bins
# Output = hist: a list of the number of nodes which fall into each bin (i.e. a histogram)
# bin_edges: a list (of len(hist) + 1) specifying the boundaries of each
# bin, which define hist

# a discrete measure of clustering coefficient.
def getNodeTriangleParticipation(Graph):
  TriadCntV = snap.TIntPrV()
  snap.GetTriadParticip(Graph, TriadCntV)
  return [(p.Val1(), p.Val2()) for p in TriadCntV]

# Purpose: Use this to get the histogram data on all WCC sizes in a given Graph
# Input: Graph
# Output: List of (x,y) pairs where x = the size of the WCC, y = # of WCCs
# of size x
def getWCCDistribution(Graph):
    ComponentDist = snap.TIntPrV()
    wcc_component_histogram = []
    snap.GetWccSzCnt(Graph, ComponentDist)
    for comp in ComponentDist:
        wcc_component_histogram.append((comp.GetVal1(), comp.GetVal2()))
    return sorted(wcc_component_histogram)

# Purpose: Use this to get the histogram data on all SCC sizes in a given Graph
# Input: Graph
# Output: List of (x,y) pairs where x = the size of the SCC, y = # of SCCs
# of size x


def getSCCDistribution(Graph):
    ComponentDist = snap.TIntPrV()
    scc_component_histogram = []
    snap.GetSccSzCnt(Graph, ComponentDist)
    for comp in ComponentDist:
        scc_component_histogram.append((comp.GetVal1(), comp.GetVal2()))
    return sorted(scc_component_histogram)

# Purpose: Gives histogram data of the PageRank scores of every node in graph
# Given a graph and and a specified number of bins, this function returns
# hist: a list of the number of nodes which fall into each bin (i.e. a histogram)
# bin_edges: a list (of len(hist) + 1) specifying the boundaries of each
# bin, which define hist


def getPageRankDistribution(Graph, bins=10):
    PRankH = snap.TIntFltH()
    snap.GetPageRank(Graph, PRankH)
    pageRankValues = []
    for node in PRankH:
        pageRankValues.append(PRankH[node])
    [hist, bin_edges] = np.histogram(pageRankValues, bins)
    return [hist, bin_edges]

# Purpose: Gives Histogram data of the sizes of all communities found in an Undirected Graph
# Given an UNDIRECTED Graph, this function returns a list of tuples (x,y)
# Where x = the size of the community derived from the Girvan-Newman community detection algorithm
# and y = The number of communities found in graph of size x


def getGirvanNewmanDistribution(Graph):
    CmtyV = snap.TCnComV()
    # Computes the Modularity and returns a list of Communities
    modularity = snap.CommunityGirvanNewman(Graph, CmtyV)
    return modularity
# 	community_size_counter = collections.Counter()
# 	for cmty in CmtyV:
# 		community_size_counter[len(cmty)]+=1
# 	community_size_distribution = sorted(community_size_counter.items(), key=itemgetter(0))
# 	return community_size_distribution

# Purpose: Provides histogram data for 2 Distributions: Edge Betweenness Centrality and Node Betweenness centrality
# Given an UNDIRECTED Graph, a fraction of nodes to analyze (NodeFrac), and the # of desired bins for the node histogram
# and the number of desired bins for the edge histogram, this function returns
# node_hist: a list of the number of nodes whose betweenness centrality fell in each bin (i.e. a histogram); len() = node_bins
# node_bin_edges: a list (of len(node_hist) + 1) specifying the boundaries of each bin, which define node_hist
# edge_hist: a list of the number of edge pairs whose betweenness centrality fell in each bin (i.e. a histogram)
# edge_bin_edges: a list (of len(edge_hist) + 1) specifying the boundaries
# of each bin, which define node_hist


def getBetweennessCentralityDistribution(Graph, NodeFrac=1.0, node_bins=10, edge_bins=10):
    Nodes = snap.TIntFltH()
    Edges = snap.TIntPrFltH()
    snap.GetBetweennessCentr(Graph, Nodes, Edges, NodeFrac)
    node_values = []
    edge_values = []
    for node in Nodes:
        node_values.append(Nodes[node])
    for edge in Edges:
        edge_values.append(Edges[edge])
    [node_hist, node_bin_edges] = np.histogram(node_values, node_bins)
    [edge_hist, edge_bin_edges] = np.histogram(edge_values, edge_bins)
    return [node_hist, node_bin_edges, edge_hist, edge_bin_edges]

# -------------------------------- Accumulator Functions -----------------

# Purpose: Use this to get the average IN-DEGREE OR OUT-DEGREE OR OVERALL DEGREE for a graph
# Input: A List of (x,y) pairs (i.e. histogram data), which is either the output of getOutDegreeDistribution,
# getInDegreeDistribution, or getDegreeDistribution
# Output: A float representing the average of the specified degree for the
# graph (depends on the input data)


def getAverageDegree(deg_distribution):
    total_degrees = sum([pair[1] * pair[0] for pair in deg_distribution])
    total_nodes = sum([pair[1] for pair in deg_distribution])
    return float(total_degrees) / total_nodes

# Purpose: Given a graph, get the average clustering coefficient as defined by Watts and Strogatz
# Input a Graph, get back a float of the average clustering coefficient.


def getAverageClusteringCoefficient(Graph):
    return snap.GetClustCf(Graph, -1)

# Computes the approximate diameter (int) of a given Graph by doing a BFS over a random set of NTestNodes
# Input is a Graph, the number of NTestNodes and whether or not the graph is directed.
# Output is the approximate diameter (an Int)
def getDiameter(Graph, NTestNodes, IsDir=False):
    return snap.GetBfsFullDiam(Graph, NTestNodes, IsDir)
