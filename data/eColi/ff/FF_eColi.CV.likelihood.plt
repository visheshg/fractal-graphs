#
# hold-out likelihood (Sat Dec  6 22:54:57 2014)
#

set title "hold-out likelihood"
set key bottom right
set autoscale
set grid
set xlabel "communities"
set ylabel "likelihood"
set tics scale 2
set terminal png size 1000,800
set output 'data/eColi/ff/FF_eColi.CV.likelihood.png'
plot 	"data/eColi/ff/FF_eColi.CV.likelihood.tab" using 1:2 title "" with linespoints pt 6
