#
# hold-out likelihood (Mon Nov 24 20:48:42 2014)
#

set title "hold-out likelihood"
set key bottom right
set autoscale
set grid
set xlabel "communities"
set ylabel "likelihood"
set tics scale 2
set terminal png size 1000,800
set output 'data/eColi/real/eColi.CV.likelihood.png'
plot 	"data/eColi/real/eColi.CV.likelihood.tab" using 1:2 title "" with linespoints pt 6
