#
# hold-out likelihood (Tue Nov 25 13:05:27 2014)
#

set title "hold-out likelihood"
set key bottom right
set autoscale
set grid
set xlabel "communities"
set ylabel "likelihood"
set tics scale 2
set terminal png size 1000,800
set output 'data/zachs_karate/real//zachs_karate.CV.likelihood.png'
plot 	"data/zachs_karate/real//zachs_karate.CV.likelihood.tab" using 1:2 title "" with linespoints pt 6
