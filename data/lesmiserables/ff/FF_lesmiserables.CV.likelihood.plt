#
# hold-out likelihood (Tue Dec  9 01:17:38 2014)
#

set title "hold-out likelihood"
set key bottom right
set autoscale
set grid
set xlabel "communities"
set ylabel "likelihood"
set tics scale 2
set terminal png size 1000,800
set output 'data/lesmiserables/ff/FF_lesmiserables.CV.likelihood.png'
plot 	"data/lesmiserables/ff/FF_lesmiserables.CV.likelihood.tab" using 1:2 title "" with linespoints pt 6
