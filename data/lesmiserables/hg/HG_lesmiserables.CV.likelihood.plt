#
# hold-out likelihood (Tue Dec  9 01:18:37 2014)
#

set title "hold-out likelihood"
set key bottom right
set autoscale
set grid
set xlabel "communities"
set ylabel "likelihood"
set tics scale 2
set terminal png size 1000,800
set output 'data/lesmiserables/hg/HG_lesmiserables.CV.likelihood.png'
plot 	"data/lesmiserables/hg/HG_lesmiserables.CV.likelihood.tab" using 1:2 title "" with linespoints pt 6
