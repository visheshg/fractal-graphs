#
# hold-out likelihood (Tue Dec  9 02:20:18 2014)
#

set title "hold-out likelihood"
set key bottom right
set autoscale
set grid
set xlabel "communities"
set ylabel "likelihood"
set tics scale 2
set terminal png size 1000,800
set output 'data/lesmiserables/kr/lesmiserables_kr.CV.likelihood.png'
plot 	"data/lesmiserables/kr/lesmiserables_kr.CV.likelihood.tab" using 1:2 title "" with linespoints pt 6
