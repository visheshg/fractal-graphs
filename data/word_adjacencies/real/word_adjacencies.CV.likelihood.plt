#
# hold-out likelihood (Tue Nov 25 13:06:41 2014)
#

set title "hold-out likelihood"
set key bottom right
set autoscale
set grid
set xlabel "communities"
set ylabel "likelihood"
set tics scale 2
set terminal png size 1000,800
set output 'data/word_adjacencies/real//word_adjacencies.CV.likelihood.png'
plot 	"data/word_adjacencies/real//word_adjacencies.CV.likelihood.tab" using 1:2 title "" with linespoints pt 6
