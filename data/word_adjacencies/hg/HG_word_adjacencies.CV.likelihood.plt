#
# hold-out likelihood (Tue Nov 25 14:51:15 2014)
#

set title "hold-out likelihood"
set key bottom right
set autoscale
set grid
set xlabel "communities"
set ylabel "likelihood"
set tics scale 2
set terminal png size 1000,800
set output 'data/word_adjacencies/hg/HG_word_adjacencies.CV.likelihood.png'
plot 	"data/word_adjacencies/hg/HG_word_adjacencies.CV.likelihood.tab" using 1:2 title "" with linespoints pt 6
