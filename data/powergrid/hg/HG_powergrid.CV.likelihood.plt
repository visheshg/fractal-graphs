#
# hold-out likelihood (Tue Nov 25 14:52:46 2014)
#

set title "hold-out likelihood"
set key bottom right
set autoscale
set grid
set xlabel "communities"
set ylabel "likelihood"
set tics scale 2
set terminal png size 1000,800
set output 'data/powergrid/hg/HG_powergrid.CV.likelihood.png'
plot 	"data/powergrid/hg/HG_powergrid.CV.likelihood.tab" using 1:2 title "" with linespoints pt 6
