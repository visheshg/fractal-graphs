#
# hold-out likelihood (Mon Dec  8 22:33:34 2014)
#

set title "hold-out likelihood"
set key bottom right
set autoscale
set grid
set xlabel "communities"
set ylabel "likelihood"
set tics scale 2
set terminal png size 1000,800
set output 'data/as20000102/ff/FF_as20000102.CV.likelihood.png'
plot 	"data/as20000102/ff/FF_as20000102.CV.likelihood.tab" using 1:2 title "" with linespoints pt 6
