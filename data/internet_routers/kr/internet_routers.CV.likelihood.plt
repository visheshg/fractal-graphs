#
# hold-out likelihood (Sun Nov 30 15:46:43 2014)
#

set title "hold-out likelihood"
set key bottom right
set autoscale
set grid
set xlabel "communities"
set ylabel "likelihood"
set tics scale 2
set terminal png size 1000,800
set output 'data/internet_routers/kr//internet_routers.CV.likelihood.png'
plot 	"data/internet_routers/kr//internet_routers.CV.likelihood.tab" using 1:2 title "" with linespoints pt 6
