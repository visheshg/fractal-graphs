#
# hold-out likelihood (Tue Nov 25 12:55:26 2014)
#

set title "hold-out likelihood"
set key bottom right
set autoscale
set grid
set xlabel "communities"
set ylabel "likelihood"
set tics scale 2
set terminal png size 1000,800
set output 'data/oregon1_010526/real//oregon1_010526.CV.likelihood.png'
plot 	"data/oregon1_010526/real//oregon1_010526.CV.likelihood.tab" using 1:2 title "" with linespoints pt 6
