import subprocess
import snap
import math
import sys
import os

graph_names = open(sys.argv[1], "r").readlines()

matrix_params = open("matrix_params.txt", "w").close()
matrix_params = open("matrix_params.txt", 'w')
kron_fit_dir = "kronfit_results/"

for name in graph_names:
    graph_name = name.rstrip('\n')
    head, tail = os.path.split(graph_name)
    fit_output = kron_fit_dir + tail[:-4]
    em_output = "KronEM-" + tail[:-4] + ".tab"
    kronfit_command = "./kronfit/kronfit -i:%s -o:%s -n0:2 -m:\"0.9 0.7; 0.5 0.2\" -gi:%d" % (
        graph_name, fit_output, int(sys.argv[2]))
    subprocess.call(kronfit_command, shell=True)
    fit_last_line = "tail -1 %s | sed 's/[^\[]*\[//g' | sed 's/]//g'" % (
        fit_output)
    fit_matrix = "\"" + \
        subprocess.check_output(fit_last_line, shell=True)[:-1] + "\""
    print "Here is the kronFit Matrix %s" % (fit_matrix)
    kronem_command = "./kronem/kronem -i:%s -o:%s -n0:2 -m:%s -ei:%d" % (
        graph_name, em_output, fit_matrix, int(sys.argv[3]))
    subprocess.call(kronem_command, shell=True)
    em_last_line = "tail -1 %s | sed 's/[^\[]*\[//g' | sed 's/]//g'" % (
        em_output)
    em_matrix = "[" + \
        subprocess.check_output(em_last_line, shell=True)[:-1] + "]"
    iterations = int(
        math.log(snap.LoadEdgeList(snap.PNGraph, graph_name).GetNodes(), 2)) + 1

    matrix_params.write(
        graph_name + "\t" + em_matrix + "\t" + str(iterations) + "\n")

matrix_params.close()
