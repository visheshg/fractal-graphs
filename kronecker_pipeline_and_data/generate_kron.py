import subprocess
import snap
import math
import sys
import os


kron_stats = open(sys.argv[1], "r").readlines()

krongen_dir = "krongen_results/"

for stats in kron_stats:
    graph_stats = stats.rstrip('\n').split('\t')
    head, tail = os.path.split(graph_stats[0])
    graph_name = tail[:-4] + "onecker.txt"
    graph_matrix = "\'" + graph_stats[1][1:-1] + "\'"
    graph_iters = int(graph_stats[2])
    krongen_command = "./krongen/krongen -o:%s -m:%s -i:%d " % (
        krongen_dir + graph_name, graph_matrix, graph_iters)
    subprocess.call(krongen_command, shell=True)
