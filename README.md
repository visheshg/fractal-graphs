Investigation of Fractal Properties of Graphs
===============================================

Overview
----------

This project aims to take the Kronecker graph model and the MNG model,
and compare/improve them.
The overall steps of this process include

* A testing harness which will take a graph and output properties and distributions
    - Used to compare model graphs to real graphs and model graphs to each other
* Ability to generate model graphs based on input parameters from real graphs
    - [Kronecker Model](http://arxiv.org/PS_cache/arxiv/pdf/0812/0812.4905v2.pdf)
    - MNG model
    - Our own model? - still in development
* Analysis tools (TBD)
    - eg: K-L distance between two distributions

Components
--------------

### stats.py
Usage:
```bash
$ python stats.py graphfile.txt
```
is a file that can be run over any graph set up in the [snappy](snap.stanford.edu/snappy) format, and will dump out a series of stastistics and some pairs of data that show distributions of certain properties. The output will be a label followed by the property, eg:
```bash
approx_diameter | 16
avg_clustering_coefficient | 0.11234
box_mass_distribution | {{1, 1}, {2, 864}, {3, 584}, {4, 323}, {5, 171}, {6, 67}, {7, 47}, {8, 34}, {9, 18}, {10, 18}, {11, 12}, {12, 7}, {13, 3}, {14, 7}, {15, 4}, {16, 6}, {17, 2}, {18, 3}, {19, 2}, {20, 2}, {21, 1}, {22, 4}, {24, 1}, {34, 1}, {35, 1}, {37, 1}, {46, 1}}
```
In general <code>|</code> will be used as a delimiter, since the distributions will have commas in them. Distribution output is in Mathematica format, which we are using to generate our graphs.

The statistics included are:

####Diameter
Shortest Path Distribution - compute all shortest paths, plot length vs number of paths of that length (bfs for every node)

Hop Plot - how many additional nodes in 1 hop, 2 hops (snap.GetNodesAtHops)
Diameter (snap.BfsFullDiam)

####Fractal
Box Counting - (random seed algorithm is easy to implement, graph covering algorithm is NP-hard but optimal) Gives us a series of graphs each of which is reduced, and we can take all other statistics on these graphs. Fractal Dimension for EACH graph. (should be self-similar as you reduce the graph)

Skeleton - (minimal spanning tree based on the edges with the highest betweenness) (Bharad)
- Compute the fractal dimension of the skeleton
- Other statistics? There are no communities, but there can be other things we do.

#### Degree
Distribution (In/Out by snap)

Average In/Out Degree

#### Clustering
WCC Distribution (snap)

SCC Distribution (for undirected graphs) (snap)

Clustering Coefficient Distribution (snap)

Clustering Coefficient (Overall Average)

####Community Size Distribution
- BIG CLAM - ask Jure (overlapping)
- Girvan-Newman (non-overlapping)
- Clique Percolation Method (overlapping) (CS)

####Centrality and Pagerank family
- Pagerank Distribution (snap)
- Betweenness Centrality Distribution (snap)
- Edge Betweenness Centrality Distribution (snap)
- SCREE Plot (snap)


Contributors
-------------
Vishesh Gupta, Bharad Raghavan, Cayman Simpson



